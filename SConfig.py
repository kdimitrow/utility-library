# THIS FILE WAS AUTOMATICALLY GENERATED. MANUAL CHANGES IN THIS FILE WILL BE LOST AFTER THE NEXT C/C++ PROJECT CHANGES!!!
SCONS_OPTIONS = {'num_jobs':'4'}
DECIDER = 'MD5'
COMP_INCLUDES_INTO_CCFLAGS = False
BUILD_ARTIFACT_NAME = 'Project'
PROJECT_TYPE = 'exe'
BUILD_CONFIGURATION = 'Debug'
TOOLCHAIN_NAME = 'linux gcc'
C_FLAGS = '-O0 -g3 -Wall -c -fmessage-length=0'
CXX_FLAGS = '-O0 -g3 -Wall -c -fmessage-length=0'
COMPILER_NAME = 'g++'
PROJECT_NAME = 'Project'
INCLUDES = []
LIBRARIES = []
LIBRARY_PATHS = []
SOURCE_PATHS = {'Project':[]}
PRE_BUILD_COMMAND = ''
PRE_BUILD_DESC = ''
POST_BUILD_COMMAND = ''
POST_BUILD_DESC = ''
COMPILER_DEFINES = {}
LINKER_FLAGS = ''
