from SCons.Script import *
import os.path

def GetObjectList(sourceList):
	objectList = []
	for source in sourceList:
		objectList.append(Object(source))
	return objectList

def GetSharedObjectList(sourceList):
	sharedObjectList = []
	for source in sourceList:
		sharedObjectList.append(SharedObject(source))
	return sharedObjectList

	

