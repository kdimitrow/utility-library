/*
 * List.cpp
 *
 *  Created on: Nov 5, 2012
 *      Author: Kalin Dimitrov
 */

#ifndef LIST_CPP_
#define LIST_CPP_

#include "../Common.h"

namespace Utils
{


template <class T>
class List
{
	template <class U>
	class Node
	{
		U data;
		Node<U>	*preview;
		Node<U> *next;
	};

public:
	List();
	List(const List<T>&);
	~List();

	void operator=(const List<T>&);
	void operator==(const List<T>&) const;

	bool Empty() const;
	const Size_t& Size() const;
	const Size_t& MaxSize() const;

	const T& Front() const;
	const T& Back() const;

	void PushFront(const T &element);
	void PopFront();
	void PushBack(const T &element);
	void PopBack();
	void Insert();
	void Erase();
	void Swap();
	void Clear();
	void Copy(const List<T>&);
	void Free();

private:


private:

	Node<T>	*first;
	Node<T> *last;

	long long count;
	long long maxCount;
};

template <class T>
inline List<T>::List()
{
	first = nullptr;
	last = nullptr;
}

template <class T>
inline List<T>::List(const List<T> &list)
{
	if(maxCount)
	{
		Free();
	}

	Copy(list);
}

template <class T>
inline List<T>::~List()
{
	Free();
}

template <class T>
inline void List<T>::operator=(const List<T> &list)
{
	Copy(list);
}

template <class T>
inline void List<T>::operator==(const List<T> &list) const
{

}

template <class T>
inline bool List<T>::Empty() const
{
	return static_cast<bool>(count);
}

template <class T>
inline const Size_t& List<T>::Size() const
{
	return count;
}

template <class T>
inline const Size_t& List<T>::MaxSize() const
{
	return maxCount;
}

template <class T>
inline const T& List<T>::Front() const
{

}

template <class T>
inline const T& List<T>::Back() const
{

}

template <class T>
inline void List<T>::PushFront(const T &element)
{

}

template <class T>
inline void List<T>::PopFront()
{

}

template <class T>
inline void List<T>::PushBack(const T &element)
{

}

template <class T>
inline void List<T>::PopBack()
{

}

template <class T>
inline void List<T>::Insert()
{

}

template <class T>
inline void List<T>::Erase()
{

}

template <class T>
inline void List<T>::Swap()
{

}

template <class T>
inline void List<T>::Clear()
{
	last = first;
	count = 0;
}

template <class T>
inline void List<T>::Copy()
{
}

template <class T>
inline void List<T>::Free(const List<T>&)
{
	Node<T> *tempNode;

	maxCount = 0;

	while(first)
	{
		tempNode = first;
		first = first->next;
		delete tempNode;
	}
}

}


#endif /* LIST_CPP_ */
