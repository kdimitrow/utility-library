/*
 * LinkedBlocks.h
 *
 *  Created on: Nov 6, 2012
 *      Author: Kalin Dimitrov
 */

#ifndef LINKEDBLOCKS_H_
#define LINKEDBLOCKS_H_

#include "../Common.h"
#include <iostream>

namespace QL
{

typedef void (*Preallocation)(unsigned &currentSize,  unsigned maxSize,  unsigned InitAllocSize);

void ConstAlloc(unsigned &currentSize, unsigned maxSize, unsigned InitAllocSize)
{
	//Do nothing
}

void LinearAlloc(unsigned &currentSize,  unsigned maxSize,  unsigned InitAllocSize)
{
	currentSize = (currentSize + InitAllocSize > maxSize)?maxSize:currentSize + InitAllocSize;
}

void ExpAlloc(unsigned &currentSize,  unsigned maxSize,  unsigned InitAllocSize)
{
	currentSize = (currentSize*2 > maxSize)?maxSize:currentSize*2;
}

template<typename U>
struct NodePool
{
	NodePool<U> *previous;
	NodePool<U> *next;
	U *pool;
	void **pointers;
	Size_t	current;
	Size_t	size;
	int filled;
};

template<typename T,unsigned InitAllocSize,unsigned MaxAllocSize,Preallocation preallocator>
class PoolAllocator
{

public:
	class Ptr
	{
	public:
		Ptr();
		Ptr(void**);
		Ptr(const Ptr&);
		~Ptr();

		void operator=(void**);
		bool operator!=(T*);
		T& operator* ();
		T* operator-> () const;

		void** GetValue() const;

	private:
		void **ptr;
	};


public:
	PoolAllocator();
	virtual	~PoolAllocator();

public:

	Ptr& Alloc();
	void DeAlloc(Ptr&);

	void Clear();
	void Free();

private:
	void CreatePool();
	void DestroyPool(NodePool<T> *nodePool);
public:
	Ptr& Assignment(void *&) const;
	void* Assignment(Ptr& ) const;

protected:
	NodePool<T> *currentPool;
	mutable Ptr	currentPtr;

	unsigned lastPoolSize;
	unsigned maxPoolSize;

};

template<typename T,unsigned InitAllocSize ,unsigned MaxAllocSize ,Preallocation preallocator >
inline PoolAllocator<T,InitAllocSize,MaxAllocSize,preallocator>::Ptr::Ptr()
{
	ptr = nullptr;
}

template<typename T,unsigned InitAllocSize ,unsigned MaxAllocSize ,Preallocation preallocator >
inline PoolAllocator<T,InitAllocSize,MaxAllocSize,preallocator>::Ptr::Ptr(void **ptr)
{
	this->ptr = ptr;
}

template<typename T,unsigned InitAllocSize ,unsigned MaxAllocSize ,Preallocation preallocator >
inline PoolAllocator<T,InitAllocSize,MaxAllocSize,preallocator>::Ptr::Ptr(const Ptr &ptr)
{
	this->ptr = ptr.ptr;
}

template<typename T,unsigned InitAllocSize ,unsigned MaxAllocSize ,Preallocation preallocator >
inline PoolAllocator<T,InitAllocSize,MaxAllocSize,preallocator>::Ptr::~Ptr()
{

}

template<typename T,unsigned InitAllocSize ,unsigned MaxAllocSize ,Preallocation preallocator >
inline void PoolAllocator<T,InitAllocSize,MaxAllocSize,preallocator>::Ptr::operator= (void **ptr)
{
	 this->ptr = ptr;
}

template<typename T,unsigned InitAllocSize ,unsigned MaxAllocSize ,Preallocation preallocator >
inline bool PoolAllocator<T,InitAllocSize,MaxAllocSize,preallocator>::Ptr::operator!= (T *ptr)
{
	 return *(this->ptr) != ptr;
}

template<typename T,unsigned InitAllocSize ,unsigned MaxAllocSize ,Preallocation preallocator >
inline T& PoolAllocator<T,InitAllocSize,MaxAllocSize,preallocator>::Ptr::operator* ()
{
	return *(static_cast<T*>(*ptr));
}

template<typename T,unsigned InitAllocSize ,unsigned MaxAllocSize ,Preallocation preallocator >
inline T* PoolAllocator<T,InitAllocSize,MaxAllocSize,preallocator>::Ptr::operator-> () const
{
	return static_cast<T*>(*ptr);
}

template<typename T,unsigned InitAllocSize ,unsigned MaxAllocSize ,Preallocation preallocator >
inline void** PoolAllocator<T,InitAllocSize,MaxAllocSize,preallocator>::Ptr::GetValue() const
{
	return ptr;
}

template<typename T,unsigned InitAllocSize ,unsigned MaxAllocSize ,Preallocation preallocator >
inline PoolAllocator<T,InitAllocSize,MaxAllocSize,preallocator>::PoolAllocator()
{
	currentPool = nullptr;
	lastPoolSize = InitAllocSize;
	maxPoolSize = MaxAllocSize;
	CreatePool();
}

template<typename T,unsigned InitAllocSize ,unsigned MaxAllocSize ,Preallocation preallocator >
inline PoolAllocator<T,InitAllocSize,MaxAllocSize,preallocator>::~PoolAllocator()
{
	Clear();
	Free();
	delete currentPool;
}

template<typename T,unsigned InitAllocSize ,unsigned MaxAllocSize ,Preallocation preallocator >
inline void PoolAllocator<T,InitAllocSize,MaxAllocSize,preallocator>::CreatePool()
{
	NodePool<T> *tempNodePool = new NodePool<T>;

	tempNodePool->size = lastPoolSize;
	tempNodePool->previous = currentPool;
	tempNodePool->next = nullptr;
	tempNodePool->current = 0;
	tempNodePool->filled = FALSE;
	tempNodePool->pool = T::Create(tempNodePool->size);
	tempNodePool->pointers = new void*[tempNodePool->size];

	for(int i = 0; i < tempNodePool->size; ++ i)
	{
		tempNodePool->pointers[i] = static_cast<void *>(tempNodePool->pool + i);
	}

	if(currentPool)
		currentPool->next = tempNodePool;

	currentPool = tempNodePool;

	preallocator(lastPoolSize,maxPoolSize,InitAllocSize);
}

template<typename T,unsigned InitAllocSize ,unsigned MaxAllocSize ,Preallocation preallocator >
inline void PoolAllocator<T,InitAllocSize,MaxAllocSize,preallocator>::DestroyPool(NodePool<T> *nodePool)
{
	T::Destroy(nodePool->pool);
	delete nodePool->pointers;
	delete nodePool;
}

template<typename T,unsigned InitAllocSize ,unsigned MaxAllocSize ,Preallocation preallocator >
inline typename PoolAllocator<T,InitAllocSize,MaxAllocSize,preallocator>::Ptr&
				PoolAllocator<T,InitAllocSize,MaxAllocSize,preallocator>::Alloc()
{
	if(currentPool->filled)
	{
		if(!(currentPool->next))
		{
			CreatePool();
		}

		else
		{
			currentPool = currentPool->next;
		}
	}

	//T *tempNode = &(currentPool->pool[currentPool->current]);
	currentPtr = &(currentPool->pointers[currentPool->current]);

	(currentPool->current)++;

	if(currentPool->current == currentPool->size)
	{
		currentPool->filled = TRUE;
	}

	//return tempNode;
	return currentPtr;
}

template<typename T,unsigned InitAllocSize ,unsigned MaxAllocSize ,Preallocation preallocator >
inline void PoolAllocator<T,InitAllocSize,MaxAllocSize,preallocator>::DeAlloc(Ptr &ptr)
{
	//T *tempNode = &(currentPool->pool[currentPool->current - 1]);
	void **tempNode = &(currentPool->pointers[currentPool->current - 1]);
	void **node = ptr.GetValue();

	if(*tempNode == *node)
	{
		(currentPool->current)--;

		if(!(currentPool->current))
		{
			if(currentPool->previous)
				currentPool = currentPool->previous;
		}

		else if(currentPool->filled)
		{
			currentPool->filled = FALSE;
		}
	}

	else
	{
		//T::Swap(node,tempNode);
		void *intercurrentNode = *node;
		*node = *tempNode;
		*tempNode = intercurrentNode;

		(currentPool->current)--;

		if(!(currentPool->current))
		{
			currentPool = currentPool->previous;
		}

		else if(currentPool->filled)
		{
			currentPool->filled = FALSE;
		}
	}
}

template<typename T,unsigned InitAllocSize ,unsigned MaxAllocSize ,Preallocation preallocator >
inline void PoolAllocator<T,InitAllocSize,MaxAllocSize,preallocator>::Clear()
{
	while(currentPool->previous)
	{
		currentPool->current = 0;
		currentPool->filled = FALSE;
		currentPool = currentPool->previous;
	}

	currentPool->current = 0;
	currentPool->filled = FALSE;
}

template<typename T,unsigned InitAllocSize ,unsigned MaxAllocSize ,Preallocation preallocator >
inline void PoolAllocator<T,InitAllocSize,MaxAllocSize,preallocator>::Free()
{
	NodePool<T> *lastNodePool = currentPool;
	NodePool<T> *tempNodePool;

	while(lastNodePool->next)
	{
		lastNodePool = lastNodePool->next;
	}

	while(lastNodePool != currentPool)
	{
		tempNodePool = lastNodePool;
		lastNodePool = tempNodePool->previous;
		DestroyPool(tempNodePool);
	}
}

template<typename T,unsigned InitAllocSize ,unsigned MaxAllocSize ,Preallocation preallocator >
inline typename PoolAllocator<T,InitAllocSize,MaxAllocSize,preallocator>::Ptr&
				PoolAllocator<T,InitAllocSize,MaxAllocSize,preallocator>::Assignment(void *&ptr) const
{
	currentPtr = &ptr;

	return currentPtr;
}

template<typename T,unsigned InitAllocSize ,unsigned MaxAllocSize ,Preallocation preallocator >
inline void* PoolAllocator<T,InitAllocSize,MaxAllocSize,preallocator>::Assignment(Ptr &ptr) const
{
	void **tempPointer = ptr.GetValue();

	if(tempPointer)
		return *(tempPointer);

	else
		return nullptr;
}

}

#endif /* LINKEDBLOCKS_H_ */
