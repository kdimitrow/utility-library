#ifndef STACK_H
#define STACK_H

#include "PoolAllocator.h"
#include "SimpleAllocator.h"

using namespace QL;

template<typename Type>
struct Node
{
	void *previous;
	Type data;

	static void Swap(Node<Type> *first,Node<Type> *second);
	static Node<Type>* Create(Size_t size);
	static void Destroy(Node<Type>* node);
};

template<typename Type>
void Node<Type>::Swap(Node<Type> *first,Node<Type> *second)
{
	Node<Type> *tempPointer;
	Type	tempData;

	tempPointer = static_cast<Node<Type> *>(first->previous);
	first->previous = second->previous;
	second->previous = tempPointer;
	tempData = first->data;
	first->data = second->data;
	second->data = tempData;
}

template<typename Type>
Node<Type>* Node<Type>::Create(Size_t size)
{
	return new Node<Type>[size];
}

template<typename Type>
void Node<Type>::Destroy(Node<Type>* node)
{
	delete [] node;
}

//template <typename Type,typename NodeType = Node<Type>, typename Allocator = PoolAllocator<NodeType,10,400000,ConstAlloc > >
template <typename Type,typename NodeType = Node<Type>, typename Allocator = SimpleAllocator<NodeType> >
class Stack
{
	typedef typename Allocator::Ptr Ptr;

public:
    Stack();
    Stack(const Stack<Type,NodeType,Allocator> &stack);
    ~Stack();

    const Stack<Type,NodeType,Allocator>& operator=(const Stack<Type,NodeType,Allocator> &stack);
    bool operator==(const Stack<Type,NodeType,Allocator> &stack) const;
    bool operator!=(const Stack<Type,NodeType,Allocator> &stack) const;

    void Push(const Type &item);
    void Pop();
    const Type& Top() const;
    bool Empty() const;
    Size_t Count() const;

    void Clear();
    void Free();

protected:
    void Copy(const Stack<Type,NodeType,Allocator> &stack);

private:
    Allocator allocator;
    Ptr top;
	Size_t count;

	Ptr ptr;

};

template <typename Type,typename NodeType, typename Allocator>
inline Stack<Type,NodeType,Allocator>::Stack()
{
	top = nullptr;
	count = 0;
}

template <typename Type,typename NodeType, typename Allocator>
inline Stack<Type,NodeType,Allocator>::Stack(const Stack<Type,NodeType,Allocator> &stack)
{
	Copy(stack);
}

template <typename Type,typename NodeType, typename Allocator>
inline Stack<Type,NodeType,Allocator>::~Stack()
{

}

template <typename Type,typename NodeType, typename Allocator>
const Stack<Type,NodeType,Allocator>& Stack<Type,NodeType,Allocator>::operator=(const Stack<Type,NodeType,Allocator> &stack)
{
	Copy(stack);
}

template <typename Type,typename NodeType, typename Allocator>
bool Stack<Type,NodeType,Allocator>::operator==(const Stack<Type,NodeType,Allocator> &stack) const
{
	if(this == &stack)
		return true;
	else
	{
		if(count != stack.count)
			return false;
		else
		{
			Ptr stackNode = stack.top;
			Ptr myNode = top;

			while(stackNode != nullptr)
			{
				if(stackNode->data != myNode->data)
				{
					return false;
				}

				stackNode = allocator.Assignment(stackNode->previous);
				myNode = allocator.Assignment(myNode->previous);
			}

			return true;
		}
	}
}

template <typename Type,typename NodeType, typename Allocator>
inline bool Stack<Type,NodeType,Allocator>::operator!=(const Stack<Type,NodeType,Allocator> &stack) const
{
	return !(*this == stack);
}

template <typename Type,typename NodeType, typename Allocator>
inline void Stack<Type,NodeType,Allocator>::Push(const Type &data)
{
	Ptr tempNode = allocator.Alloc();

	tempNode->data = data;
	tempNode->previous = allocator.Assignment(top);
	top = tempNode;
	count++;
}

template <typename Type,typename NodeType, typename Allocator>
inline void Stack<Type,NodeType,Allocator>::Pop()
{
	Ptr tempNode = top;

	top = allocator.Assignment(top->previous);
	allocator.DeAlloc(tempNode);
	count--;
}

template <typename Type,typename NodeType, typename Allocator>
inline const Type &Stack<Type,NodeType,Allocator>::Top() const
{
	return (top->data);
}

template <typename Type,typename NodeType, typename Allocator>
inline bool Stack<Type,NodeType,Allocator>::Empty() const
{
	return !(static_cast<bool>(count));
}

template <typename Type,typename NodeType, typename Allocator>
inline Size_t Stack<Type,NodeType,Allocator>::Count() const
{
	return count;
}

template <typename Type,typename NodeType, typename Allocator>
inline void Stack<Type,NodeType,Allocator>::Clear()
{
	allocator.Clear();
	count = 0;
	top = nullptr;
}

template <typename Type,typename NodeType, typename Allocator>
inline void Stack<Type,NodeType,Allocator>::Free()
{
	allocator.Free();
}

template <typename Type,typename NodeType, typename Allocator>
inline void Stack<Type,NodeType,Allocator>::Copy(const Stack<Type,NodeType,Allocator> &stack)
{
	Ptr stackNode = stack.top;
	Ptr tempNode;
	Ptr nextNode;

	Clear();

	if(stackNode != nullptr)
	{
		count = stack.count;
		top = allocator.Alloc();
		top->data = stackNode->data;
		stackNode = allocator.Assignment(stackNode->previous);
		nextNode = top;

		while(stackNode != 0)
		{
			tempNode = allocator.Alloc();
			tempNode->data = stackNode->data;
			stackNode = allocator.Assignment(stackNode->previous);
			nextNode->previous = allocator.Assignment(tempNode);
			nextNode = tempNode;
		}

		nextNode->previous = nullptr;
	}

	else
	{
		top = nullptr;
		count = 0;
	}
}

#endif // STACK_H
