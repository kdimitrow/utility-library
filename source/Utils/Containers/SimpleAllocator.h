/*
 * LinkedBlocks.h
 *
 *  Created on: Nov 16, 2012
 *      Author: Kalin Dimitrov
 */

#ifndef SIMPLE_ALLOCATOR_H_
#define SIMPLE_ALLOCATOR_H_

#include "../Common.h"

template<typename TypeNode>
class SimpleAllocator
{
public:
	typedef TypeNode* Ptr;
public:
	SimpleAllocator();
	~SimpleAllocator();
public:
	Ptr Alloc();
	void DeAlloc(Ptr);

	void Clear();
	void Free();

public:
	static Ptr Assignment(void *);
	static void* Assignment(Ptr );
};


template<typename TypeNode>
inline SimpleAllocator<TypeNode>::SimpleAllocator()
{

}

template<typename TypeNode>
inline SimpleAllocator<TypeNode>::~SimpleAllocator()
{

}

template<typename TypeNode>
inline typename SimpleAllocator<TypeNode>::Ptr SimpleAllocator<TypeNode>::Alloc()
{
	return new TypeNode;
}

template<typename TypeNode>
inline void SimpleAllocator<TypeNode>::DeAlloc(Ptr node)
{
	delete node;
}

template<typename TypeNode>
inline void SimpleAllocator<TypeNode>::Clear()
{

}


template<typename TypeNode>
inline void SimpleAllocator<TypeNode>::Free()
{

}

template<typename TypeNode>
inline typename SimpleAllocator<TypeNode>::Ptr SimpleAllocator<TypeNode>::Assignment(void *ptr)
{
	return static_cast<Ptr>(ptr);
}

template<typename TypeNode>
inline void* SimpleAllocator<TypeNode>::Assignment(Ptr ptr)
{
	return static_cast<void*>(ptr);
}








#endif /*SIMPLE_ALLOCATOR_H_*/
