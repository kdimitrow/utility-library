/*
 * LinkedBlocks.h
 *
 *  Created on: Nov 6, 2012
 *      Author: Kalin Dimitrov
 */

#ifndef LINKEDBLOCKS_H_
#define LINKEDBLOCKS_H_

#include "../Common.h"

namespace Utils
{

typedef void (*Preallocation)(unsigned &currentSize,  unsigned maxSize,  unsigned InitAllocSize);

void ConstAlloc(unsigned &currentSize, unsigned maxSize, unsigned InitAllocSize)
{
	//Do nothing
}

void LinearAlloc(unsigned &currentSize,  unsigned maxSize,  unsigned InitAllocSize)
{
	currentSize = (currentSize + InitAllocSize > maxSize)?maxSize:currentSize + InitAllocSize;
}

void ExpAlloc(unsigned &currentSize,  unsigned maxSize,  unsigned InitAllocSize)
{
	currentSize = (currentSize*2 > maxSize)?maxSize:currentSize*2;
}

template<typename U>
struct NodePool
{
	NodePool<U> *previous;
	NodePool<U> *next;
	U *pool;
	Size_t	current;
	Size_t	size;
	int filled;
};

template<typename T,unsigned InitAllocSize,unsigned MaxAllocSize,Preallocation preallocator>
class LinkedBlocks
{


public:
	LinkedBlocks();
	virtual	~LinkedBlocks();

public:

	T* Alloc();
	void DeAlloc(T*);

	void Clear();
	void Free();

private:
	void CreatePool();
	void DestroyPool(NodePool<T> *pool);

protected:
	NodePool<T> *currentPool;

	unsigned lastPoolSize;
	unsigned maxPoolSize;

};


template<typename T,unsigned InitAllocSize ,unsigned MaxAllocSize ,Preallocation preallocator >
inline LinkedBlocks<T,InitAllocSize,MaxAllocSize,preallocator>::LinkedBlocks()
{
	currentPool = nullptr;
	lastPoolSize = InitAllocSize;
	maxPoolSize = MaxAllocSize;
	CreatePool();
}

template<typename T,unsigned InitAllocSize ,unsigned MaxAllocSize ,Preallocation preallocator >
inline LinkedBlocks<T,InitAllocSize,MaxAllocSize,preallocator>::~LinkedBlocks()
{
	Clear();
	Free();
	delete currentPool;
}

template<typename T,unsigned InitAllocSize ,unsigned MaxAllocSize ,Preallocation preallocator >
inline void LinkedBlocks<T,InitAllocSize,MaxAllocSize,preallocator>::CreatePool()
{
	NodePool<T> *tempNodePool = new NodePool<T>;

	tempNodePool->size = lastPoolSize;
	tempNodePool->previous = currentPool;
	tempNodePool->next = nullptr;
	tempNodePool->current = 0;
	tempNodePool->filled = FALSE;
	tempNodePool->pool = T::Create(tempNodePool->size);

	if(currentPool)
		currentPool->next = tempNodePool;

	currentPool = tempNodePool;

	preallocator(lastPoolSize,maxPoolSize,InitAllocSize);
}

template<typename T,unsigned InitAllocSize ,unsigned MaxAllocSize ,Preallocation preallocator >
inline void LinkedBlocks<T,InitAllocSize,MaxAllocSize,preallocator>::DestroyPool(NodePool<T> *pool)
{
	T::Destroy(pool->pool);
	delete pool;
}

template<typename T,unsigned InitAllocSize ,unsigned MaxAllocSize ,Preallocation preallocator >
inline T* LinkedBlocks<T,InitAllocSize,MaxAllocSize,preallocator>::Alloc()
{
	if(currentPool->filled)
	{
		if(!(currentPool->next))
		{
			CreatePool();
		}

		else
		{
			currentPool = currentPool->next;
		}
	}

	T *tempNode = &(currentPool->pool[currentPool->current]);

	(currentPool->current)++;

	if(currentPool->current == currentPool->size)
	{
		currentPool->filled = TRUE;
	}

	return tempNode;
}

template<typename T,unsigned InitAllocSize ,unsigned MaxAllocSize ,Preallocation preallocator >
inline void LinkedBlocks<T,InitAllocSize,MaxAllocSize,preallocator>::DeAlloc(T *node)
{
	T *tempNode = &(currentPool->pool[currentPool->current - 1]);

	if(node == tempNode)
	{
		(currentPool->current)--;

		if(!(currentPool->current))
		{
			if(currentPool->previous)
				currentPool = currentPool->previous;
		}

		else if(currentPool->filled)
		{
			currentPool->filled = FALSE;
		}
	}

	else
	{
		//node->data = tempNode->data;
		//node->next = tempNode->next;
		//node->previous = tempNode->previous;
		T::Swap(node,tempNode);

		(currentPool->current)--;

		if(!(currentPool->current))
		{
			currentPool = currentPool->previous;
		}

		else if(currentPool->filled)
		{
			currentPool->filled = FALSE;
		}
	}
}

template<typename T,unsigned InitAllocSize ,unsigned MaxAllocSize ,Preallocation preallocator >
inline void LinkedBlocks<T,InitAllocSize,MaxAllocSize,preallocator>::Clear()
{
	while(currentPool->previous)
	{
		currentPool->current = 0;
		currentPool->filled = FALSE;
		currentPool = currentPool->previous;
	}

	currentPool->current = 0;
	currentPool->filled = FALSE;
}

template<typename T,unsigned InitAllocSize ,unsigned MaxAllocSize ,Preallocation preallocator >
inline void LinkedBlocks<T,InitAllocSize,MaxAllocSize,preallocator>::Free()
{
	NodePool<T> *lastNodePool = currentPool;
	NodePool<T> *tempNodePool;

	while(lastNodePool->next)
	{
		lastNodePool = lastNodePool->next;
	}

	while(lastNodePool != currentPool)
	{
		tempNodePool = lastNodePool;
		lastNodePool = tempNodePool->previous;
		DestroyPool(tempNodePool);
	}
}

}

#endif /* LINKEDBLOCKS_H_ */
