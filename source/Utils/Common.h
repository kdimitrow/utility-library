/*
 * Common.h
 *
 *  Created on: Nov 5, 2012
 *      Author: Kalin Dimitrov
 */

#ifndef COMMON_H_
#define COMMON_H_

#if __cplusplus > 199711L

#else
	#define nullptr 0
#endif
#define TRUE	1
#define FALSE	0

typedef long long Size_t;

#endif /* COMMON_H_ */
