/*
 * main.cpp
 *
 *  Created on: Nov 5, 2012
 *      Author: default
 */

#include <iostream>
#include <stack>
#include "../Utils/Containers/Stack.h"
//#include "Stack.h"
#include <ctime>

using namespace std;
using namespace QL;

#define NUM_OF_ITERATIONS 1000000

void AllocatorsTest();
void TestStack();

int main()
{
	//AllocatorsTest();
	TestStack();

    return 0;
}

void AllocatorsTest()
{
	/*{
		//Pool allocator
		PoolAllocator<Node<int>,10,100,ConstAlloc > poolAllocator;
		stack<Node<int>*> allocatorPointers;

		Node<int> *tempNode;
		for(int i = 0; i < 10; ++i)
		{
			for(int i = 0;i < NUM_OF_ITERATIONS; ++i)
			{
				tempNode = poolAllocator.Alloc();
				allocatorPointers.push(tempNode);
				//std::cout<<std::dec<<"Print address: "<<reinterpret_cast<int>(tempNode)<<std::endl;
			}

			while(!allocatorPointers.empty())
			{
				poolAllocator.DeAlloc(allocatorPointers.top());
				allocatorPointers.pop();
			}
		}

		cout<<"Finish"<<endl;
	}
	*/
}

void TestStack()
{
	std::cout<<"Stack test relatively to std::stack and integer type value"<<std::endl;
	std::cout<<"Number of iteration "<< NUM_OF_ITERATIONS<<std::endl<<std::endl;
	std::cout<<"1. Precision test"<<std::endl;

	{

		std::cout<<"1.1 Correct value test"<<std::endl;

		Stack<int> myStack;
		std::stack<int> stdStack;
		bool passTheTest = true;
		long long iteration = 0;

		for(int i = 0; i < NUM_OF_ITERATIONS; ++i)
		{
				myStack.Push(i);
				stdStack.push(i);
		}

		while(!stdStack.empty())
		{
			iteration++;

			if(myStack.Top() != stdStack.top())
			{
				std::cout<<"myStack value "<<myStack.Top()<<std::endl;
				std::cout<<"stdStack value "<<stdStack.top()<<std::endl;
				passTheTest = false;
				break;
			}

			else
			{
				myStack.Pop();
				stdStack.pop();
			}
		}

		if(passTheTest && (iteration == NUM_OF_ITERATIONS))
		{
			std::cout<<"Pass the test"<<std::endl;
		}

		else
		{
			std::cout<<"Fail the test"<<std::endl;
			std::cout<<"iteration "<<iteration<<std::endl;
		}

	}

	{
		std::cout<<"1.1 Copy constructor test"<<std::endl;

		Stack<int> myStack1;
		bool passTheTest = true;
		long long iteration = 0;

		for(int i = 0; i < NUM_OF_ITERATIONS; ++i)
		{
			myStack1.Push(i);
		}

		Stack<int> myStack2 = myStack1;
		int z = myStack2.Top();
		myStack2.Pop();
		myStack2.Push(z);

		while(!myStack1.Empty())
		{
			iteration++;

			if(myStack1.Top() != myStack2.Top())
			{
				std::cout<<"it: "<<iteration<<std::endl;
				std::cout<<"myStack1 value "<<myStack1.Top()<<std::endl;
				std::cout<<"myStack2 value "<<myStack2.Top()<<std::endl;
				passTheTest = false;
				break;
			}

			else
			{
				myStack1.Pop();
				myStack2.Pop();
			}
		}

		if(passTheTest && (iteration == NUM_OF_ITERATIONS))
		{
			std::cout<<"Pass the test"<<std::endl;
		}

		else
		{
			std::cout<<"Fail the test"<<std::endl;
			std::cout<<"iteration "<<iteration<<std::endl;
		}
	}

	{
		std::cout<<"1.2 Assignment operator test"<<std::endl;

		Stack<int> myStack1;
		Stack<int> myStack2;
		bool passTheTest = true;
		long long iteration = 0;


		for(int i = 0; i < NUM_OF_ITERATIONS; ++i)
		{
			myStack1.Push(i);
		}

		myStack2 = myStack1;

		while(!myStack1.Empty())
		{
			iteration++;

			if(myStack1.Top() != myStack2.Top())
			{
				std::cout<<"it: "<<iteration<<std::endl;
				std::cout<<"myStack1 value "<<myStack1.Top()<<std::endl;
				std::cout<<"myStack2 value "<<myStack2.Top()<<std::endl;
				passTheTest = false;
				break;
			}

			else
			{
				myStack1.Pop();
				myStack2.Pop();
			}
		}

		if(passTheTest && (iteration == NUM_OF_ITERATIONS))
		{
			std::cout<<"Pass the test"<<std::endl;
		}

		else
		{
			std::cout<<"Fail the test"<<std::endl;
			std::cout<<"iteration "<<iteration<<std::endl;
		}
	}

	{
		std::cout<<"1.3 \"Equal to\" and \"Not equal to\" operators test"<<std::endl;

		Stack<int> myStack1;
		Stack<int> myStack2;
		Stack<int> myStack3;
		bool passTheTest = true;

		for(int i = 0; i < NUM_OF_ITERATIONS; ++i)
		{
			myStack1.Push(i);
			myStack2.Push(i);
		}

		passTheTest = passTheTest && (myStack1 == myStack1);
		passTheTest = passTheTest && (myStack1 == myStack2);
		passTheTest = passTheTest && !(myStack1 == myStack3);
		myStack2.Pop();
		myStack2.Push(NUM_OF_ITERATIONS);
		passTheTest = passTheTest && !(myStack1 == myStack2);

		if(passTheTest)
		{
			std::cout<<"Pass the test"<<std::endl;
		}

		else
		{
			std::cout<<"Fail the test"<<std::endl;
		}
	}

	{
		std::cout<<"1.4 \"Count\" test"<<std::endl;

		Stack<int> myStack;

		for(int i = 0; i < NUM_OF_ITERATIONS; ++i)
		{
			myStack.Push(i);
		}

		if(myStack.Count() == NUM_OF_ITERATIONS)
		{
			std::cout<<"Pass the test"<<std::endl;
		}

		else
		{
			std::cout<<"Fail the test"<<std::endl;
		}

	}

	{
		std::cout<<"1.5 \"Clear\" test"<<std::endl;

		Stack<int> myStack1;
		Stack<int> myStack2;

		for(int i = 0; i < NUM_OF_ITERATIONS; ++i)
		{
			myStack1.Push(i);
			myStack2.Push(i);
		}

		myStack2.Clear();

		for(int i = 0; i < NUM_OF_ITERATIONS; ++i)
		{
			myStack2.Push(i);
		}




		if(myStack1 == myStack2)
		{
			std::cout<<"Pass the test"<<std::endl;
		}

		else
		{
			std::cout<<"Fail the test"<<std::endl;
		}
	}

	{
		std::cout<<"1.6 Stable test"<<std::endl;

		Stack<int> myStack;
		std::stack<int> stdStack;
		bool stable = true;
		long long iteration = 0;
		int numberOfChecking = 10;

		for(int i = 0; i < numberOfChecking; ++i)
		{
			iteration = 0;

			for(int i = 0; i < NUM_OF_ITERATIONS; ++i)
			{
					myStack.Push(i);
					stdStack.push(i);
			}

			while(!stdStack.empty())
			{
				iteration++;

				if(myStack.Top() != stdStack.top())
				{
					std::cout<<"myStack value "<<myStack.Top()<<std::endl;
					std::cout<<"stdStack value "<<stdStack.top()<<std::endl;
					stable = false;
					break;
				}

				else
				{
					myStack.Pop();
					stdStack.pop();
				}
			}
		}

		if(stable && (iteration == NUM_OF_ITERATIONS))
		{
			std::cout<<"Pass the test"<<std::endl;
		}

		else
		{
			std::cout<<"Fail the test"<<std::endl;
			std::cout<<"iteration "<<iteration<<std::endl;
		}

	}

	std::cout<<"1. Speed test"<<std::endl;
	{
		Stack<int> myStack;
		std::stack<int> stdStack;
		clock_t begin;
		clock_t end;
		double myStackPush;
		double myStackPop;
		double stdPush;
		double stdPop;
		int counter;

		begin = clock();

		for(counter = 0; counter < NUM_OF_ITERATIONS; ++counter)
		{
			myStack.Push(counter);
		}

		end = clock();

		myStackPush = double(end - begin)/CLOCKS_PER_SEC;

		begin = clock();

		while(!myStack.Empty())
		{
			myStack.Top();
			myStack.Pop();
		}

		end = clock();

		myStackPop = double(end - begin)/CLOCKS_PER_SEC;



		begin = clock();

		for(counter = 0; counter < NUM_OF_ITERATIONS; ++counter)
		{
			stdStack.push(counter);
		}

		end = clock();

		stdPush = double(end - begin)/CLOCKS_PER_SEC;

		begin = clock();

		while(!stdStack.empty())
		{
			stdStack.top();
			stdStack.pop();
		}

		end = clock();

		stdPop = double(end - begin)/CLOCKS_PER_SEC;


		std::cout<<"myStack push time "<<myStackPush<<std::endl;
		std::cout<<"myStack pop and top time "<<myStackPop<<std::endl;
		std::cout<<"myStack total time "<<myStackPop + myStackPush<<std::endl<<std::endl;

		std::cout<<"stdStack push time "<<stdPush<<std::endl;
		std::cout<<"stdStack pop and top time "<<stdPop<<std::endl;
		std::cout<<"stdStack total time "<<stdPush + stdPop<<std::endl<<std::endl;
	}

}
































